#!/bin/sh
echo "Init git submodules"
git submodule update --init

echo "Copy theme assets"
cp -r custom/themes/fonts reveal.js/dist/theme
cp -r custom/themes/images reveal.js/dist/theme
cp custom/themes/*.scss reveal.js/css/theme/source

echo "Build theme"
if ! command -v npm 2>&1 /dev/null
then
    echo "npm command not found, try fallback using docker"
    command -v docker >/dev/null 2>&1 || \
        { echo >&2 "No npm or docker found. Can not do anything"; exit 1; }
    docker run -it --rm --user "$(id -u)":"$(id -g)" \
        -v "$(pwd)/reveal.js:/reveal.js" -w /reveal.js node:10-slim \
        bash -c "npm install && npm run build -- css-themes"
else
    # run commands in subshell, because changing of directory
    (
        cd reveal.js || \
            { echo >&2 "failed to switch into reveal.js directory"; exit 1; }
        npm install
        npm run build -- css-themes
    )
fi

echo "Now open 'presentation.html' in your browser: \
        file://$(pwd)/presentation.html"
